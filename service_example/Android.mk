# Copyright 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := brillo_example_service
LOCAL_SRC_FILES := \
    brillo_example_service.cpp \
    android/brillo/example/IAlertCallback.aidl \
    android/brillo/example/IExampleService.aidl
LOCAL_AIDL_INCLUDES := $(LOCAL_PATH)
LOCAL_CFLAGS := -Wall
LOCAL_SHARED_LIBRARIES := \
    libbinder \
    libbrillo \
    libbrillo-binder \
    libchrome \
    libutils
# Uncomment the following line to have this service started automatically on
# boot by init.
#LOCAL_INIT_RC := example_service.rc
include $(BUILD_EXECUTABLE)

include $(CLEAR_VARS)
LOCAL_MODULE := brillo_example_client
LOCAL_SRC_FILES := \
    brillo_example_client.cpp \
    android/brillo/example/IAlertCallback.aidl \
    android/brillo/example/IExampleService.aidl
LOCAL_AIDL_INCLUDES := $(LOCAL_PATH)
LOCAL_CFLAGS := -Wall
LOCAL_SHARED_LIBRARIES := \
    libbinder \
    libbrillo \
    libbrillo-binder \
    libchrome \
    libcutils \
    libutils
include $(BUILD_EXECUTABLE)
