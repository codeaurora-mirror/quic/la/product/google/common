// Copyright 2015 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
//
// Example of using i2c devices with Brillo.
//
// These use /dev/i2c-6. On the dragonboard 410c, this device maps to i2c1 on
// pins 19 (SCL) and 21 (SDA).


#include <android-base/logging.h>

#include "i2c_device.h"


// Example of using the mcp23017 IO port expander.
//
// On the expander, pin0 is an output pin, and pin1 is an input pin, simply
// connected to gnd through an led or switch.
//
// 0 -> LED -> gnd
// 1 -> SWITCH -> gnd
//
// i2c is connected directly to the board. Pin1 will use the internal pullup
// resistor of the mcp23017.
//
void io_expander_demo() {
  // Opens the device at i2c1 at the default address (the default address is
  // calculated when A0 A1 A2 pins are all pulled low).
  I2CDevice mcp23017("/dev/i2c-6", 0x20);
  mcp23017.Begin();
  // Sets the pin1 as an input, the rest as output.
  mcp23017.SetRegister(0x0,  0b00000010);
  // Sets the internal pullup resistor for pin1.
  mcp23017.SetRegister(0xC,  0b00000010);
  // Sets the initial values of the GPIOA register.
  mcp23017.SetRegister(0x12, 0b00000010);
  uint8_t register_value;
  while(1) {
    // Reads the GPIOA register.
    mcp23017.GetRegister(0x12, register_value);
    // Decides if switch is on based on whether pin1 is low.
    uint8_t is_switch_on = (register_value & 0b00000010) >> 1;
    // Sets the GPIO register at pin0 to reflect the switch state on pin1.
    mcp23017.SetRegister(0x12, is_switch_on ^ 0b10000001);
    LOG(INFO) << (is_switch_on ? "On." : "Off.");
    // Waiting a bit is sensible.
    usleep(0x10000);
  }
}


// Example of using the ds1307 real time clock.
//
// i2c is connected directly to the board.
//
void rtc_demo() {
  // Opens the device at i2c1 at the default address (the default address is
  // calculated when A0 A1 A2 pins are all pulled low).
  I2CDevice ds1307("/dev/i2c-6", 0x68);
  ds1307.Begin();
  // Needs three bytes, one for each of seconds, minutes, hours.
  uint8_t seconds_register_value = 0x0;
  uint8_t minutes_register_value = 0x0;
  uint8_t hours_register_value   = 0x0;
  char time_string[10];
  while (1) {
    ds1307.GetRegister(0x0, seconds_register_value);
    ds1307.GetRegister(0x1, minutes_register_value);
    ds1307.GetRegister(0x2, hours_register_value);
    // Munges the values based on their storage scheme.
    int seconds = 10 * ((seconds_register_value & 0b01110000) >> 4) +
        (seconds_register_value & 0b00001111);
    int minutes = 10 * ((minutes_register_value & 0b01110000) >> 4) +
        (minutes_register_value & 0b00001111);
    int hours   = 10 * ((hours_register_value & 0b00110000) >> 4) +
        (hours_register_value & 0b00001111);
    sprintf(time_string, "%02d:%02d:%02d", hours, minutes, seconds);
    LOG(INFO) << "Time is:" << time_string;
    sleep(1);
  }
}


// Example of reading the ds1307 real time clock, and writing to the mcp23017
// port expander.
//
// This demo uses both devices at the same time.
void binary_watch_demo() {
  I2CDevice mcp23017("/dev/i2c-6", 0x20);
  mcp23017.Begin();
  mcp23017.SetRegister(0x0,  0b00000000);
  mcp23017.SetRegister(0x12, 0b00000000);
  I2CDevice ds1307("/dev/i2c-6", 0x68);
  ds1307.Begin();
  uint8_t seconds_register_value = 0x0;
  while(1) {
    ds1307.GetRegister(0x0, seconds_register_value);
    mcp23017.SetRegister(0x12, seconds_register_value << 2);
    sleep(1);
  }
}


int main(int argc __unused, char **argv __unused) {
  //io_expander_demo();
  //rtc_demo();
  binary_watch_demo();
  return 0;
}
