#include <unistd.h>
#include <android-base/logging.h>

int main(int argc __unused, char **argv __unused) {
  static const char* const leds[] = {
    "/sys/class/leds/led1/brightness",
    "/sys/class/leds/led2/brightness",
    "/sys/class/leds/led3/brightness",
    "/sys/class/leds/boot/brightness"
  };

  LOG(INFO) << "starting blinking LEDs";
  int i = 0;
  for(;;) {
    FILE *fp;
    fp = fopen(leds[i], "w");
    if (fp == NULL) {
      PLOG(ERROR) << "Error opening file";
      return 0;
    } else {
      fputs("255", fp);
      fseek(fp, 0, SEEK_SET);
      sleep(1);
      fputs("0", fp);
      fclose(fp);
      LOG(INFO) << "toggling LED " << i + 1 << " on/off";
      i++;
      i = i % 4;
    }
  }
  LOG(INFO) << "exiting";
  return 0;
}
